// challenge No.1
function changeWord (selectedText,changedText,text){
    const newStr =
    text.replace(selectedText,changedText,text);
    return newStr;
}
const kalimat1= 'Andini sangat mencintai kamu selamanya'
const kalimat2= 'Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu'

console.log(changeWord('mencintai', 'membenci', kalimat1))
console.log(changeWord('bromo', 'semeru', kalimat2))

// challenge No.2
function checkTypeNumber(givenNumber){
    var hasil;
    if (Number.isInteger(givenNumber)){
        if(givenNumber % 2 == 0){
            return hasil = 'GENAP'
        }
        else if (givenNumber % 2 == 1){ 
            return hasil = 'GANJIL'
        }
    }else{
        return hasil = 'ERROR: Invalid Data Type'
    }
}
console.log(checkTypeNumber(10))
console.log(checkTypeNumber(3))
console.log(checkTypeNumber('3'))
console.log(checkTypeNumber({}))
console.log(checkTypeNumber([]))
console.log(checkTypeNumber())

// challenge No.3
function checkEmail(email){
    let hasil;
        let formatEmail = new RegExp(/^[a-zA-Z0-9][\-_\.\+\!\#\$\%\&\'\*\/\=\?\^\`\{\|]{0,1}([a-zA-Z0-9][\-_\.\+\!\#\$\%\&\'\*\/\=\?\^\`\{\|]{0,1})*[a-zA-Z0-9]@[a-zA-Z0-9][-\.]{0,1}([a-zA-Z][-\.]{0,1})*[a-zA-Z0-9]\.[a-zA-Z0-9]{1,}([\.\-]{0,1}[a-zA-Z]){0,}[a-zA-Z0-9]{0,}$/i)
        if (typeof email === 'string'){
            if (formatEmail.test(email)){
                return hasil = 'VALID'
        } else if (!formatEmail.test(email)) {
            var tanpaAt = email.indexOf("@");
                var tanpaTitik = email.indexOf(".")
          if(tanpaAt < 1 && tanpaTitik < 1){
            return hasil = 'ERROR : Invalid Format'    
          }else {
            return hasil = 'INVALID'
          }
       }
    } else {
        return hasil = 'ERROR : Invalid Data Type'
    }
}
    console.log(checkEmail('apranata@binar.co.id'))
    console.log(checkEmail('apranata@binar.com'))
    console.log(checkEmail('apranata@binar'))
    console.log(checkEmail('apranata'))
    console.log(checkEmail(3322))
    console.log(checkEmail())

// challenge No.4
function isValidPassword(email) {
    let regex = new RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/)
    if (typeof email === "string") {
        if (regex.test(email) === true) {
            return true
        } else if (regex.test(email) === false) {
            return false
        }
    } else return "ERROR : NULL"
}
    console.log(isValidPassword('Meong2021'))
    console.log(isValidPassword('meong2021'))
    console.log(isValidPassword('@eong2021'))
    console.log(isValidPassword('Meong2'))
    console.log(isValidPassword('0'))
    console.log(isValidPassword())

// challenge No.5
function getSplitName(personName) {
    if (typeof personName === "string") {
        let nameArr = personName.split(" ");
        if (nameArr.length === 3) {
            return {
                firstName: nameArr[0],
                middleName: nameArr[1],
                lastName: nameArr[2],
            };
        } else if (nameArr.length === 2) {
            return {
                firstName: nameArr[0],
                middleName: null,
                lastName: nameArr[1],
            };
        } else if (nameArr.length === 1) {
            return {
                firstName: nameArr[0],
                middleName: null,
                lastName: null,
            };
        } else {
            return "ERROR : This Function Only For 3 Characters Name";
        }
    } else {
        return "ERROR : String Data Type Only";
    }
}

console.log(getSplitName("Aldi Daniela Pernata"));
console.log(getSplitName("Dwi Kuncoro"));
console.log(getSplitName("Aurora"));
console.log(getSplitName("Aurora Aurelia Sukma Darma"));
console.log(getSplitName(8));

// challenge no.6
const getAngkaTerbesarKedua = (arr) => {
    if (typeof arr === 'object') {
        sortedArr = arr.sort();
        return sortedArr[sortedArr.length-2];
    }else{
        return 'ERROR';
    }
};

const dataAngka = [9,4,7,7,4,3,2,2,8]

console.log(getAngkaTerbesarKedua(dataAngka))
console.log(getAngkaTerbesarKedua(0))
console.log(getAngkaTerbesarKedua())

// challenge No.7
const dataPenjualanPakAldi = [
    {
        namaProduct : 'Sepatu Futsal Nike Vapor Academy 8',
        hargaSatuan : 760000,
        kategori : "Sepatu Sport",
        totalTerjual : 90,
    },
    {
        namaProduct : 'Sepatu Warrior Tristan Black Brown High',
        hargaSatuan : 960000,
        kategori : "Sepatu Sneaker",
        totalTerjual : 37,
    },
    {
        namaProduct : 'Sepatu Warrior Tristan Maroon High',
        hargaSatuan : 360000,
        kategori : "Sepatu Sneaker",
        totalTerjual : 90,
    },
    {
        namaProduct : 'Sepatu Warrior Rainbow Tosca Corduroy',
        hargaSatuan : 120000,
        kategori : "Sepatu Sneaker",
        totalTerjual : 90,
    }
]
 
const hitungTotalPenjualan = (arr) => arr.reduce((prev,cur) => prev+cur.totalTerjual, 0)    

console.log(hitungTotalPenjualan(dataPenjualanPakAldi))

// challenge No.8
const dataPenjualanNovel = [
    {
        idProduct: 'BOOK002941',
        namaProduk: 'Laskar Pelangi',
        penulis: 'Andrea Hirata',
        hargaBeli: 55000,
        hargaJual: 68000,
        totalTerjual: 20,
        sisaStok: 56,   
    },
    {
        idProduct: 'BOOK002421',
        namaProduk: 'Pulang-Pergi',
        penulis: 'Tare Liye',
        hargaBeli: 60000,
        hargaJual: 86000,
        totalTerjual: 150,
        sisaStok: 17,
    },
    {
        idProduct: 'BOOK002941',
        namaProduk: 'Garis Waktu',
        penulis: 'Fiersa Besari',
        hargaBeli: 67000,
        hargaJual: 99000,
        totalTerjual: 213,
        sisaStok: 5,
    },
    {
        idProduct: 'BOOK002351',
        namaProduk: 'Selamat Tinggal',
        penulis: 'Tare Liye',
        hargaBeli: 75000,
        hargaJual: 103000,
        totalTerjual: 171,
        sisaStok: 20,
    }
]
function getInfoPenjualan(dataPenjualan){
    let result = {}
    const totalKeuntungan =  dataPenjualan.map((item) => {
        return item.totalTerjual * (item.hargaJual - item.hargaBeli);
    });
    const hasilTotalKeuntungan = totalKeuntungan.reduce((prev, curr) => {
        return prev + curr
    });
    const totalModal = dataPenjualan.map((item) => {
        return item.hargaBeli * (item.sisaStok + item.totalTerjual);
    });
    const hasilTotalModal = totalModal.reduce((prev, curr) => {
        return prev + curr
    });  
    const presentaseKeuntungan = dataPenjualan.map((item) => {
        return (hasilTotalKeuntungan / hasilTotalModal) * 100;
    });
    const hasilPresentaseKeuntungan = presentaseKeuntungan.reduce((prev, curr) => {
        return prev + curr
    });  
    const produkBukuTerlaris = dataPenjualan.reduce((prev, curr) => {
        return curr.totalTerjual > (prev.totalTerjual || 0) ? curr : prev.namaProduk;
      });
    const penulisTerlaris = dataPenjualan.reduce((prev, curr) => {
        const index = prev.findIndex(search => search.penulis === curr.penulis)

        if (index !== -1) {
            const data = [...prev]
            data[index].totalTerjual += curr.totalTerjual
            return [...data]
        }

        return [...prev, curr]

    }, [])

    const hasilPenulisTerlaris = penulisTerlaris.reduce((prev, curr) => {
        return curr.totalTerjual > (prev.totalTerjual || 0) ? curr : prev.penulis
    });
    
    result.totalKeuntungan = `Rp.${new Intl.NumberFormat('id-ID').format(Math.floor(hasilTotalKeuntungan))}`;
    result.totalModal = `Rp.${new Intl.NumberFormat('id-ID').format(Math.floor(hasilTotalModal))}`;
    result.persentaseKeuntungan = hasilPresentaseKeuntungan.toFixed(2)  + '%';
    result.produkBukuTerlaris = produkBukuTerlaris;
    result.penulisTerlaris = hasilPenulisTerlaris;

    return result
    }

    console.log(getInfoPenjualan(dataPenjualanNovel));